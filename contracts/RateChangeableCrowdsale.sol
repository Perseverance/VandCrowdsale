pragma solidity ^0.4.15;

import '../node_modules/zeppelin-solidity/contracts/crowdsale/Crowdsale.sol';
import '../node_modules/zeppelin-solidity/contracts/ownership/Ownable.sol';


contract RateChangeableCrowdsale is Crowdsale, Ownable {

    event LogRateChanged(uint newRate);

    function changeRate(uint _rate) onlyOwner {
        require(_rate > 0);
        rate = _rate;

        LogRateChanged(rate);
    }
    

}