pragma solidity ^0.4.15;

import '../node_modules/zeppelin-solidity/contracts/token/StandardToken.sol';
import '../node_modules/zeppelin-solidity/contracts/token/MintableToken.sol';
import '../node_modules/zeppelin-solidity/contracts/token/PausableToken.sol';
import "./HumanStandardToken.sol";


contract VandToken is PausableToken, MintableToken, HumanStandardToken {


    function VandToken() HumanStandardToken(0, "Vand Token", 21, "VAND") { }

}