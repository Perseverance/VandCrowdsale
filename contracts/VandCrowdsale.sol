pragma solidity ^0.4.15;

import '../node_modules/zeppelin-solidity/contracts/crowdsale/CappedCrowdsale.sol';
import '../node_modules/zeppelin-solidity/contracts/crowdsale/FinalizableCrowdsale.sol';
import './RateChangeableCrowdsale.sol';
import './VandToken.sol';


contract VandCrowdsale is CappedCrowdsale, FinalizableCrowdsale, RateChangeableCrowdsale {


    function VandCrowdsale(
        uint256 startTime, 
        uint256 endTime, 
        uint256 rate, 
        address wallet, 
        uint256 cap)

        Crowdsale(startTime, endTime, rate, wallet) 
        CappedCrowdsale(cap)
        {

    }

    function createTokenContract() internal returns (MintableToken) {
        return new VandToken();
    }

    function finalization() internal {
        token.transferOwnership(owner);
    }

}