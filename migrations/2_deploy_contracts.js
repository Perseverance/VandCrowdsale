var VandCrowdsale = artifacts.require("./VandCrowdsale.sol");

function getFutureTimestamp(plusMinutes) {
  let date = new Date();
  date.setMinutes(date.getMinutes() + plusMinutes)
  let timestamp = +date;
  timestamp = Math.ceil(timestamp / 1000);
  return timestamp;
}

module.exports = function (deployer) {

  let start = getFutureTimestamp(10);
  let end = getFutureTimestamp(15);
  const rate = 200;
  const hardCap = 1000;
  const wallet = "0xa5d530fdfa784db95105dd67236cb1f9ea5283ae";

  deployer.deploy(VandCrowdsale, start, end, rate, wallet, hardCap);
};
