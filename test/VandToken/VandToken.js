const VandToken = artifacts.require("./VandToken.sol");
const expectThrow = require('../util').expectThrow;

contract('VandToken', function(accounts) {

    let VTInstance;

    const _owner = accounts[0];
    const _notOwner = accounts[1];

    const _name = "Vand Token";
    const _initialTotalSupply = 0;
    const _decimals = 21;
    const _symbol = "VAND";

    describe("creating vand token", () => {
        beforeEach(async function() {
            VTInstance = await VandToken.new({
                from: _owner
            });
        })

        it("should set owner correctly", async function() {
            let owner = await VTInstance.owner.call();

            assert.strictEqual(owner, _owner, "The expected owner is not set");
        })

        it("should have no totalSupply", async function() {
            let totalSupply = await VTInstance.totalSupply.call();

            assert(totalSupply.eq(_initialTotalSupply), `The contract has initial supply of : ${totalSupply.toNumber()}`);
        })

        it("should set the name correctly", async function() {
            let name = await VTInstance.name.call();

            assert.strictEqual(name, _name, `The contract name is incorrect : ${name}`);
        })

        it("should set the symbol correctly", async function() {
            let symbol = await VTInstance.symbol.call();

            assert.strictEqual(symbol, _symbol, `The contract symbol is incorrect : ${symbol}`);
        })

        it("should set the decimals correctly", async function() {
            let decimals = await VTInstance.decimals.call();

            assert(decimals.eq(_decimals), `The contract decimals are incorrect : ${decimals.toNumber()}`);
        })


    });


});