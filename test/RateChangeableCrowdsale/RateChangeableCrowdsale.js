const VandCrowdsale = artifacts.require("./VandCrowdsale.sol");
const expectThrow = require('../util').expectThrow;
const getTimestampPlusSeconds = require('../util').getTimestampPlusSeconds;

contract('VandCrowdsaleRateChangeableCrowdsale', function(accounts) {

    let VCInstance;

    const _owner = accounts[0];
    const _notOwner = accounts[5];
    let start;
    let end;
    const hardCap = 1000;
    const wallet = accounts[9];

    const _rate = 200;
    const _newRate = 400;

    describe("changing rate", () => {

        beforeEach(async function() {

            start = getTimestampPlusSeconds(0);
            end = getTimestampPlusSeconds(30);

            VCInstance = await VandCrowdsale.new(start, end, _rate, wallet, hardCap, {
                from: _owner
            });

        })

        it("should change rate correctly", async function() {
            await VCInstance.changeRate(_newRate);

            let rate = await VCInstance.rate.call();

            assert(rate.eq(_newRate), "The rate was not changed");
        })

        it("should throw if non owner tries to change", async function() {
            await expectThrow(VCInstance.changeRate.call(_newRate, {
                from: _notOwner
            }));
        });
        it("should throw if rate set is 0", async function() {
            await expectThrow(VCInstance.changeRate.call(0));
        });

        it("should emit event on rate changed", async function() {
            const expectedEvent = 'LogRateChanged';
            let result = await VCInstance.changeRate(_newRate);
            assert.lengthOf(result.logs, 1, "There should be 1 event emitted from rate change!");
            assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
        });

    });


});