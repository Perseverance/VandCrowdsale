const VandCrowdsale = artifacts.require("./VandCrowdsale.sol");
const VandToken = artifacts.require("./VandToken.sol");
const expectThrow = require('../util').expectThrow;
const getTimestampPlusSeconds = require('../util').getTimestampPlusSeconds;
const getTimeoutPromise = require('../util').getTimeoutPromise;

contract('VandCrowdsale', function(accounts) {

    let VCInstance;
    let _startTime;
    let _endTime;

    const _owner = accounts[0];
    const _wallet = accounts[9];

    const _rate = 200;
    const _newRate = 400;
    const _cap = 1000;

    describe("initializing crowsale", () => {

        beforeEach(async function() {

            _startTime = getTimestampPlusSeconds(0);
            _endTime = getTimestampPlusSeconds(30);

            VCInstance = await VandCrowdsale.new(_startTime, _endTime, _rate, _wallet, _cap, {
                from: _owner
            });

        })

        it("should set initial values correctly", async function() {

            let startTime = await VCInstance.startTime.call();
            let endTime = await VCInstance.endTime.call();
            let wallet = await VCInstance.wallet.call();
            let rate = await VCInstance.rate.call();
            let cap = await VCInstance.cap.call();

            assert(startTime.eq(_startTime), "The start time is incorrect");
            assert(endTime.eq(_endTime), "The start time is incorrect");
            assert(rate.eq(_rate), "The start time is incorrect");
            assert(cap.eq(_cap), "The start time is incorrect");
            assert.strictEqual(wallet, _wallet, "The start time is incorrect");
        })

        it("should create Vand Token", async function() {
            let token = await VCInstance.token.call();
            assert(token.length > 0, "Token length is 0");
            assert(token != "0x0");
        })


    });

    describe("testing crowdsale process", () => {
        let VTInstance;
        const _symbol = "VAND";
        const secondsUntilEnd = 4;

        beforeEach(async function() {

            _startTime = getTimestampPlusSeconds(0);
            _endTime = getTimestampPlusSeconds(secondsUntilEnd);

            VCInstance = await VandCrowdsale.new(_startTime, _endTime, _rate, _wallet, _cap, {
                from: _owner
            });

            let tokenAddress = await VCInstance.token.call();

            VTInstance = VandToken.at(tokenAddress);

            await getTimeoutPromise(1);

        })

        it("should create the correct token", async function() {
            let tokenSymbol = await VTInstance.symbol.call();
            assert.equal(tokenSymbol, _symbol, "It has not created token with the correct symbol");
        })

        it("should transfer tokens to buyer", async function() {
            const weiSent = 10;
            await VCInstance.buyTokens(_owner, {
                value: weiSent
            })

            let balance = await VTInstance.balanceOf.call(_owner);

            assert(balance.eq(weiSent * _rate), "The balance was not correct based on the rate and weiSent");
        })

        it("should end on cap", async function() {

            await VCInstance.buyTokens(_owner, {
                value: _cap
            })

            let ended = await VCInstance.hasEnded.call();
            assert(ended, "Crowdsale has not ended");
        })

        it("should throw if try to finalize before end", async function() {
            await expectThrow(VCInstance.finalize.call());
        })

        it("should transfer ownership of the token correctly on cap finish", async function() {
            let initialOwner = await VTInstance.owner.call();
            await VCInstance.buyTokens(_owner, {
                value: _cap
            })
            await VCInstance.finalize();
            let afterOwner = await VTInstance.owner.call();

            assert(initialOwner != afterOwner, "The owner has not changed");
            assert.equal(afterOwner, _owner, "The owner was not set to the crowdsale owner");
        })

        it("should transfer ownership of the token correctly on time finish", async function() {
            let initialOwner = await VTInstance.owner.call();
            await getTimeoutPromise(secondsUntilEnd + 1);
            await VCInstance.finalize();
            let afterOwner = await VTInstance.owner.call();

            assert(initialOwner != afterOwner, "The owner has not changed");
            assert.equal(afterOwner, _owner, "The owner was not set to the crowdsale owner");
        })

    })


});