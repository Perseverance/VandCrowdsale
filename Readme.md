# Vand Crowdsale and Token contracts

## The code
So basically the code is based on Zeppelin. It's extended to include rate change and transfer token contract ownership to the crowdsale owner at the end of the crowdsale.

### The Crowdsale
The crowdsale has the following params

- `uint256 startTime` - The UNIX timestamp when the crowdsale starts. Must be in the future.
- `uint256 endTime` - The UNIX timestamp when the crowdsale ends. Must be in the future and after `startTime`
- `uint256 rate` - The initial rate. How many base units (this should take into account the token percision) per 1 wei.
- `address wallet` - The address that will receive the funds from the crowdsale.
- `uint256 cap` - Market cap of the crowdsale in **wei**

### The Token

At this time I have embedded all the params in the Token itself, here are the params used:

*These are to be discussed*

- Initial supply - 0
- Symbol of the token: VAND
- Name of the token: Vand Token
- Percision of the token - 21 - Matches the wei=>ether percision


How many decimals to show. Setting this to 21 means having 1000000000000000000000 base units per VAND. I will call them Base Vand Units or BVAND. Meaning 0.9872 VAND = 987200000000000000000 BVAND. It's like comparing 1 wei to 1000 ether. If we asume rate of 1 in the crowdsale 1 wei = 1000 BVAND.

The base unit is what is going to be distributed in the crowdsale and mintend via the token. So if you want to distribute 1500 VAND token per 1 ether (1000000000000000000 wei) you need to set the rate param to 1500 * 1000 = 1500000. Here is the math:

```
1500000 BVAND per wei * 1000000000000000000 wei = 1500000 * 1000000000000000000 BVAND

1500000 * 1000000000000000000 BVAND = 1500 * 1000000000000000000000 BVAND

1000000000000000000000 BVAND = 1 VAND

//Comming from the two above
1500 * 1000000000000000000000 BVAND = 1500 * 1 VAND = 1500 VAND
```


## Notes for the owner/operator

### Presale reduced rate
Deploy the crowdsale contract in the reduced pre-sale rate.

### Set the normal Crowdsale rate
Use the `changeRate(uint _rate)` function of the crowdsale to set the rate to the normal crowdsale rate

### Things to do after the Crowdsale

#### Finalize the crowdsale
After the crowdsale is complete (either through the time limit or through the cap) call the `finalize()` function of the contract in order to finalize the crowdsale and transfer the ownership of the Token contract to the crowdsale contract owner.

#### Minting the employee reserved tokens
If you want to mint the employee tokens as % of all tokens, calculate the new tokens (keep in mind that minting mints in base unit) based on the `totalSupply` field of the token contract and then use the `mint(address _to, uint256 _amount)` function of the token contract. **Doing this before doing the previous point will result in failure.**
